import Foundation

struct TVShow {
    var title: String
    var year: Int
    var seasons: Int

    init(title: String, year: Int, seasons: Int) {
        self.title = title
        self.year = year
        self.seasons = seasons
    }
}
