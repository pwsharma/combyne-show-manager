import UIKit

class TVShowTableViewCell: UITableViewCell, ReusableView {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var yearLabel: UILabel!
    @IBOutlet private weak var seasonsLabel: UILabel!

    var tvShow: TVShow? {
        didSet {
            guard let _tvShow = tvShow else { return }
            self.titleLabel.text = _tvShow.title.description
            self.yearLabel.text = _tvShow.year.description
            self.seasonsLabel.text = _tvShow.seasons.description
        }
    }

    static func getReuseIdentifier() -> String {
        return "TVShowTableViewCell"
    }
}
