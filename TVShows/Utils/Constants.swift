import Foundation
import UIKit

struct Constants {
    struct Keys {
        static let title = "title"
        static let year = "year"
        static let seasons = "seasons"
    }

    struct FileNames {
        static let parseCredentials = "parseCredentials"
    }

    struct DesignProperties {
        static let cornerRadiusView = CGFloat(10)
    }
}
