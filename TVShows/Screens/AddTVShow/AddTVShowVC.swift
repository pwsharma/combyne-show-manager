import Foundation
import UIKit

class AddTVShowVC: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var titleTextField: BorderedTextField!
    @IBOutlet private weak var yearTextField: BorderedTextField!
    @IBOutlet private weak var seasonsTextField: BorderedTextField!

    // MARK: - Variables

    var presenter: AddTVShowPresenter!

    // MARK: - Lifecycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialise Presenter
        self.presenter = AddTVShowPresenter(service: self)
        // Setup UI
        self.setup()
    }

    // MARK: - Helper Methods

    fileprivate func setup() {
        self.navigationItem.title = "Add new TV show"
    }

    // MARK: - Action Methods

    @IBAction func saveButtonClicked() {
        self.presenter.addTVShow(
            title: self.titleTextField.text, year: self.yearTextField.text, seasons: self.seasonsTextField.text
        )
    }
}

extension AddTVShowVC: AddTVShowDelegate {
    func showErrorAlert(error: String) {
        showAlert(title: "Error", message: error)
    }

    func didAddTVShow() {
        self.navigationController?.popViewController(animated: true)
    }
}
