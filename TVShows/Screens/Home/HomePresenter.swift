import Foundation

protocol HomeDelegate: ViewService {
    func didUpdateShowsList()
}

class HomePresenter {

    enum HomeSection: Int, CaseIterable {
        case slides
        case showsList
    }

    let service: HomeDelegate?

    private(set) var sections: [HomeSection] = HomeSection.allCases

    private(set) var tvShows = [TVShow]() {
        didSet {
            self.service?.didUpdateShowsList()
        }
    }

    // MARK: - Initialised Methods

    init(service: HomeDelegate) {
        self.service = service
    }

    // MARK: - Controller Methods

    func getRowsCount(section: HomeSection) -> Int {
        switch section {
        case .slides:
            return 0
        case .showsList:
            return tvShows.count
        }
    }

    func synchronizeTVShows() {
        CloudStoreManager.shared.getListOfTVShows(offset: 0, numberOfShows: 5) { result in
            switch result {
            case .failure(let error):
                self.service?.showErrorAlert(error: error.localizedDescription)
            case .success(let tvShows):
                self.tvShows = tvShows
            }
        }
    }
}
