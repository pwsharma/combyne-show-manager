import Foundation
import UIKit
import MaterialComponents

class HomeVC: UIViewController {

    // MARK: Outlets

    @IBOutlet private weak var tvShowTableView: BaseTableView!

    // MARK: Variables

    var presenter: HomePresenter!

    // MARK: Lifecycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialise presenter
        self.presenter = HomePresenter(service: self)
        self.presenter.synchronizeTVShows()
        // Setup table view for the list of shows
        self.setupTableView()
    }

    // MARK: Helper Methods

    fileprivate func setupTableView() {
        let nib = UINib(nibName: "TVShowTableViewCell", bundle: nil)
        self.tvShowTableView.register(nib, forCellReuseIdentifier: TVShowTableViewCell.getReuseIdentifier())

        self.tvShowTableView.dataSource = self
        self.tvShowTableView.rowHeight = UITableView.automaticDimension
        self.tvShowTableView.estimatedRowHeight = 100
    }
}

extension HomeVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let homeSection = HomePresenter.HomeSection(rawValue: section) else { return 0 }
        return presenter.getRowsCount(section: homeSection)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TVShowTableViewCell.getReuseIdentifier())
            as? TVShowTableViewCell else { return TVShowTableViewCell() }
        cell.tvShow = presenter.tvShows[indexPath.row]
        return cell
    }
}

extension HomeVC: HomeDelegate {
    func didUpdateShowsList() {
        self.tvShowTableView.reloadData()
    }

    func showErrorAlert(error: String) {
        self.showAlert(message: error)
    }
}
