import Foundation

protocol ViewService {
    func showErrorAlert(error: String)
}
