import Foundation

typealias CloudStoreServiceResultAddShow = (_ result: Result<TVShow, CloudStoreError>) -> Void
typealias CloudStoreServiceResultGetShows = (_ result: Result<[TVShow], CloudStoreError>) -> Void

enum CloudStoreError: Error {
    case badAddRequest
    case showsNotFound
}

protocol CloudStoreService {
    func addTVShow(tvShow: TVShow, result: @escaping CloudStoreServiceResultAddShow)
    func getTVShows(offset: Int, numberOfShows: Int, result: @escaping CloudStoreServiceResultGetShows)
}
