import Foundation
import Parse

class ParseCloudService: CloudStoreService {

    func addTVShow(tvShow: TVShow, result: @escaping CloudStoreServiceResultAddShow) {

        let tvShowData = PFObject(className: "TVShowList")

        tvShowData[Constants.Keys.title] = tvShow.title
        tvShowData[Constants.Keys.year] = tvShow.year
        tvShowData[Constants.Keys.seasons] = tvShow.seasons

        tvShowData.saveInBackground { (success, error)  in
            if let _ = error {
                result(.failure(.badAddRequest))
                return
            }
            result(.success(tvShow))
        }
    }

    func getTVShows(offset: Int, numberOfShows: Int, result: @escaping CloudStoreServiceResultGetShows) {
        let query = PFQuery(className:"TVShowList")

        query.findObjectsInBackground { (objects: [PFObject]?, error: Error?) in
            if let _ = error {
                result(.failure(.showsNotFound))
            } else if let objects = objects {
                let tvShows = objects.compactMap({
                    TVShow(
                        title: $0[Constants.Keys.title] as? String ?? "",
                        year: $0[Constants.Keys.year] as? Int ?? 0,
                        seasons: $0[Constants.Keys.seasons] as? Int ?? 0
                    )
                })
                result(.success(tvShows))
            }
        }
    }
}
