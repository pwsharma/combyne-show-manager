import Foundation

class CloudStoreManager {

    static let shared = CloudStoreManager()

    let service: CloudStoreService? = ParseCloudService()

    private init() {}

    func addTVShow(tvShow: TVShow, result: @escaping CloudStoreServiceResultAddShow) {
        self.service?.addTVShow(tvShow: tvShow, result: result)
    }

    func getListOfTVShows(offset: Int, numberOfShows: Int, result: @escaping CloudStoreServiceResultGetShows) {
        self.service?.getTVShows(offset: offset, numberOfShows: numberOfShows, result: result)
    }
}
